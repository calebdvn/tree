#include <vector>
#include <iostream>
#include <stdexcept>
#include <math.h>

using std::cout;
using std::vector;

class Tree;

typedef vector<vector<bool> > LabelSet;

Tree makeTree(vector<Tree> subtrees, bool label);
Tree makeTree(bool label);
vector<bool> shortenPath(vector<bool> *p);
LabelSet getSuffixesOfLabelsWithPrefix(LabelSet *labels, bool prefix);
// bool checkEveryBipartitionOfLabelSet(Tree t1, Tree t2, LabelSet labels);
// bool checkThatequallyLabeledSubtreesHaveLabelSets(Tree t1, Tree t2, LabelSet labels);
bool caseAllEqual(Tree *t1, Tree *t2, Tree *t3, LabelSet *shortenedLabels);
bool caseTwoEqual(Tree *tEq1, Tree *tEq2, Tree *tNeq, LabelSet *lEq, LabelSet *lNeq);
void vout(vector<bool> v);
bool checkLeaves(Tree *t1, Tree *t2, Tree *t3, LabelSet *labels);
bool ternaryTreeContainsBinaryTreeWithLabelSet(Tree *ternary, LabelSet *labels);
void nChooseKSets(unsigned int n, unsigned int k, unsigned int idx, vector<int> *tuple, unsigned int offset, vector<vector<int> > *kTuples);
vector<LabelSet> nTupleLabelSetsLengthK(int n, int k);
vector<bool> generateNthBinaryStringLengthK(int n, int k);
void printLabelSet(LabelSet l);

class Tree {
  public:
    bool leaf;
    bool label;
    vector<Tree> subTrees;
    Tree(bool label){
      this->label    = label;
      leaf           = true;
    }
    Tree(vector<Tree> subTrees, bool label){
      this->label    = label;
      this->subTrees = subTrees;
      leaf           = false;
    }
};

//I am sorry for the following abominations wrappers
Tree makeTree(vector<Tree> subTrees, bool label){
  Tree t(subTrees,label);
  return t;
}

Tree makeTree(bool label){
  Tree t(label);
  return t;
}

vector<bool> shortenPath(vector<bool> * p){
  vector<bool> pshort(p->begin()+1, p->end());
  return pshort;
}

vector<bool> generateNthBinaryStringLengthK(int n, int k){
  vector<bool> bitString;
  for( int i=0; i<k; i++ )
    bitString.push_back(((int)(n/pow(2,i)))%2==0);
  return bitString;
}

vector<LabelSet> nTupleLabelSetsLengthK(int n, int k){
  vector<vector<int> > allCombos;
  unsigned int lengthKBitStrings = (int)pow(2,k);
  vector<int> tupleInit;
  nChooseKSets(lengthKBitStrings,n,0,&tupleInit,0,&allCombos);
  vector<LabelSet> nTupleLabelSets;
  for(vector<int> combo : allCombos){
    LabelSet labels;
    for(int bitStringNumber : combo)
      labels.push_back(generateNthBinaryStringLengthK(bitStringNumber,k));
    nTupleLabelSets.push_back(labels);
  }
  return nTupleLabelSets;
}

LabelSet getSuffixesOfLabelsWithPrefix(LabelSet * labels, bool prefix){
  LabelSet suffix;
  for(vector<bool> pth : *labels)
    if(pth.at(0)==prefix)
      suffix.push_back(shortenPath(&pth));
  return suffix;
}

void nChooseKSets(unsigned int n, unsigned int k, unsigned int idx, vector<int> *tuple, unsigned int offset, vector<vector<int> > *kTuples)
{
	if (idx == k){
        kTuples->push_back(*tuple);
        return;
	}

	if (offset >= n)
		return;

  if( (unsigned int)idx >= tuple->size()){
    tuple->push_back(offset);
  }else{
    tuple->at(idx) = offset;
  }

	nChooseKSets(n, k, idx+1, tuple, offset+1,kTuples);
	nChooseKSets(n, k, idx, tuple, offset+1,kTuples);
}

bool hasPath(Tree *tree, vector<bool> *pth){
  for(Tree subtree : tree->subTrees){
    if(subtree.leaf==true)
      return (subtree.label == pth->at(0));
    if(subtree.label == pth->at(0)){
      if(pth->begin() == pth->end())
        throw std::invalid_argument("vector<bool> label too short!");
      vector<bool> shortenedpath = shortenPath(pth);
      if(hasPath(&subtree,&shortenedpath))
        return true;
    }
  }
  return false;
}

bool caseAllEqual(Tree *t1, Tree *t2, Tree *t3, LabelSet *shortenedLabels){
  int sum = 0;
  sum += ternaryTreeContainsBinaryTreeWithLabelSet(t1,shortenedLabels);
  sum += ternaryTreeContainsBinaryTreeWithLabelSet(t2,shortenedLabels);
  if(sum==2) return true;
  sum += ternaryTreeContainsBinaryTreeWithLabelSet(t3,shortenedLabels);
  if(sum==2) return true;
  return false;
}

bool caseTwoEqual(Tree *tEq1, Tree *tEq2, Tree *tNeq, LabelSet *lEq, LabelSet *lNeq){
  if(lEq->size()==0) return false;
  if (lNeq->size()>0){
    if(ternaryTreeContainsBinaryTreeWithLabelSet(tNeq,lNeq)){
      if(ternaryTreeContainsBinaryTreeWithLabelSet(tEq1,lEq))
        return true;
      if(ternaryTreeContainsBinaryTreeWithLabelSet(tEq2,lEq))
        return true;
    }
    return false;
  }else{
    return ternaryTreeContainsBinaryTreeWithLabelSet(tEq1,lEq) && ternaryTreeContainsBinaryTreeWithLabelSet(tEq2,lEq);
  }
}

void vout(vector<bool> v){
  for(bool u : v)
    cout << u << " ";
  cout << "\n";
}

bool checkLeaves(Tree *t1, Tree *t2, Tree *t3, LabelSet *labels){
  if(labels->size()==2)
    return true;
  if(labels->size()==1){
    bool l1 = t1->label;
    bool l2 = t2->label;
    bool l3 = t3->label;
    bool pL = labels->at(0).at(0);
    return  (((pL==l1) && (pL==l2)) ||
             ((pL==l1) && (pL==l3)) ||
             ((pL==l2) && (pL==l3)) );
  }

  if(labels->at(0).size()>1) throw std::invalid_argument("Weird Label Size>1");
  if(labels->size()>2) throw std::invalid_argument("Weird Labels Size>2");
  throw std::invalid_argument("Arr... Thar' be dragons");
}

bool ternaryTreeContainsBinaryTreeWithLabelSet(Tree *ternary, LabelSet *labels){
  if(labels->size()==0) return false;
  if(labels->at(0).size()==0) throw std::invalid_argument("Weird Label Size");

  Tree *t1 = &ternary->subTrees.at(0);
  Tree *t2 = &ternary->subTrees.at(1);
  Tree *t3 = &ternary->subTrees.at(2);

  if(t1->leaf)
    return checkLeaves(t1,t2,t3,labels);

  LabelSet shortenedLabels0 = getSuffixesOfLabelsWithPrefix(labels,false);
  LabelSet shortenedLabels1 = getSuffixesOfLabelsWithPrefix(labels,true);

  if(t1->label==t2->label && t1->label == t3->label){
    if(t1->label==true){
      return caseAllEqual(t1,t2,t3,&shortenedLabels1);
    }else{
      return caseAllEqual(t1,t2,t3,&shortenedLabels0);
    }
    return false;
  }else{
    if(t1->label==t2->label){
      if(t1->label==true){
        return caseTwoEqual(t1, t2, t3, &shortenedLabels1, &shortenedLabels0);
      }else{
        return caseTwoEqual(t1, t2, t3, &shortenedLabels0, &shortenedLabels1);
      }
    }
    if(t1->label==t3->label){
      if(t1->label==true){
        return caseTwoEqual(t1, t3, t2, &shortenedLabels1, &shortenedLabels0);
      }else{
        return caseTwoEqual(t1, t3, t2, &shortenedLabels0, &shortenedLabels1);
      }
    }
    if(t2->label==t3->label){
      if(t2->label==true){
        return caseTwoEqual(t2, t3, t1, &shortenedLabels1, &shortenedLabels0);
      }else{
        return caseTwoEqual(t2, t3, t1, &shortenedLabels0, &shortenedLabels1);
      }
    }
    throw "Somehow, t1, t2, and t3 all have distinct labels!";
    return false;
  }
}

int getDepth(Tree *t){
  if(t->leaf)
    return 0;
  return 1+getDepth(&t->subTrees.at(0));
}

LabelSet getSmallestLabelSetOfBinaryTreeInTernaryTree(Tree ternary){
  int depth = getDepth(&ternary);
  LabelSet empty({});
  for(int i=1; i<=pow(2,depth); i++){
    vector<LabelSet> testLabelSets = nTupleLabelSetsLengthK(i, depth);
    for(LabelSet labels : testLabelSets){
      if(ternaryTreeContainsBinaryTreeWithLabelSet(&ternary, &labels))
        return labels;
    }
  }
  return empty;
}

void printLabelSet(LabelSet l){
  for(vector<bool> vb : l){
    vout(vb);
  }
}

int main(){
  Tree leaf1(1);
  Tree leaf0(0);

  Tree L1(1);
  Tree L0(0);

  Tree A11 = makeTree({L1,L1,L1},1);
  Tree A10 = makeTree({L0,L0,L0},1);
  Tree A01 = makeTree({L1,L1,L1},0);
  Tree A00 = makeTree({L0,L0,L0},0);

  Tree B1 = makeTree({A11,A10,A00},1);
  Tree B2 = makeTree({A01,A01,A01},1);
  Tree B3 = makeTree({A11,A10,A11},0);

  Tree B4 = makeTree({A00,A01,A11},1);
  Tree B5 = makeTree({A10,A10,A10},1);
  Tree B6 = makeTree({A00,A01,A10},0);

  Tree B7 = makeTree({A11,A10,A00},0);
  Tree B8 = makeTree({A01,A01,A11},0);
  Tree B9 = makeTree({A11,A10,A11},1);

  Tree C1 = makeTree({B1,B2,B3},1);
  Tree C2 = makeTree({B4,B5,B6},1);
  Tree C3 = makeTree({B7,B8,B9},1);

  Tree D = makeTree({C1,C2,C3},1);

  LabelSet smallestLabelSet = getSmallestLabelSetOfBinaryTreeInTernaryTree(D);
  cout << "smallest tree size: "<< smallestLabelSet.size() << "\n";
  cout << "smallest label set:\n";
  printLabelSet(smallestLabelSet);

  return 0;
}
