CC := g++
CFLAGS := -Wall -Wextra -Werror -Wfatal-errors -g -std=c++11
#-p -O0 -no-pie
#-O3 -ffast-math -m64

all: tree tree.o

tree.o: tree.cpp
	$(CC) $(CFLAGS) -c tree.cpp
tree: tree.o
	$(CC) $(CFLAGS) -o tree tree.o

clean:
	rm -fr tree *.o
